# Metabolic Pathway Profiler - command line simulation tool for metabolic models

The "Metabolic Pathway Profiler" command line tool provides two simulation methods for metabolic systems, the Max-min Driving Force (MDF) method and Enzyme Cost Minimization (ECM). Input data (model structure, bounds on variables, and kinetic and thermodynamic parameters) are given in SBtab format, wrapped in a Combine Archive (.omex file). The tool is based on python code from the [eQuilibrator project](https://gitlab.com/equilibrator/equilibrator-pathway) and was developed to integrate MDF and ECM simulations into [BioSimulators](https://biosimulators.org/).

## Simulation methods

### Max-min Driving Force method (MDF)
For a given metabolic network model, the MDF method (Noor et al. 2014) determines plausible metabolite concentrations and thermodynamic forces.
Flux directions, equilibrium constants (or equivalently, standard Gibbs free energies of reactions) and admissible metabolite concentration ranges are given as input data. The method applies the principle that low thermodynamic forces must be avoided, and maximizes the minimum thermodynamic force across the entire network.

* Code: [gitlab.com/equilibrator/equilibrator-pathway](https://gitlab.com/equilibrator/equilibrator-pathway)

### Enzyme Cost Minimization (ECM)
For a given metabolic network model, the [Enzyme Cost Minimization](https://www.metabolic-economics.de/enzyme-cost-minimization/) method (Noor et al. 2016) determines plausible metabolite and enzyme concentrations (which determine thermodynamic forces and enzyme catalytic rates). Fluxes, enzyme kinetic constants, admissible metabolite concentration ranges, and enzyme cost weights (e.g. enzyme molecular masses) are given as input data. The method applies the principle that high enzyme (or enzyme plus metabolite) concentrations must be avoided, and maximizes a weighted sum of enzyme and  metabolite concentrations.

* Code: [gitlab.com/equilibrator/equilibrator-pathway](https://gitlab.com/equilibrator/equilibrator-pathway)

## Installation

### Option 1: Running from source

In order to install and test the CLI tool:
- clone the repository to your local drive: `git clone https://gitlab.com/equilibrator/metabolic-pathway-profiler.git`
- change directory: `cd metabolic-pathway-profiler`
- create a virtual environment: `python -m venv venv` (note: on some systems the `python` command should be replaced with `python3`)
- (within bash) activate the virtual environment: `source venv/bin/activate`
- install dependencies: `pip install -r requirements.txt`

Our example model (of *E. coli* central metabolism) was taken from Noor et al. 2016.

You can run each of the two algorithms (MDF or ECM) on this example using:
- `python -m biosimulators_mpp.cli -i examples/example-ecoli-mdf/ecoli_noor_2016_mdf.omex -o examples/example-ecoli-mdf/results`
- `python -m biosimulators_mpp.cli -i examples/example-ecoli-ecm/ecoli_noor_2016_ecm.omex -o examples/example-ecoli-ecm/results`

### Option 2: Running using Docker

The scripts were tested only on Ubuntu Linux (21.10) and might not work on other systems.
Instead, you can run MPP using Docker:
- clone the repository to your local drive
- if needed, install Docker: [Windows](https://docs.docker.com/desktop/windows/install/), [OSX](https://docs.docker.com/desktop/mac/install/), [Ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- run the example script: `./run_docker_example.sh` (NOTE: this might take 10-20 minutes, and requires large downloads of ~1.3GB)
- the result files can be found in `examples/example-ecoli-ecm/results/` and `examples/example-ecoli-mdf/results/`

## File formats

### SBtab data format for Metabolic Pathway Profiler

[SBtab](https://sbtab.net/) (Lubitz et al. 2016) is a standardised table format for systems biology. Python Code for handling SBtab can be found on [github.com/tlubitz/SBtab](https://github.com/tlubitz/SBtab).

An SBtab file contains a number of tables following a predefined format. The tables types and nomenclature used for Metabolic Pathway Profiler are described in the subfolder ``sbtab-format``.

### Combine Archive format for Metabolic Pathway Profiler

[Combine Archive](https://combinearchive.org/index/) (.omex) files (Bergmann et al. 2014) are zip files containing all necessary files for a simulation project, as well as some standardized metadata files.

Our Combine Archive file format contains 4 files:
  - ``manifest.xml`` - manifest file (as required for combine archives)
  - ``metadata.rdf`` - metadata file in rdf format (as required for combine archives)
  - ``model.tsv`` - simulation input file (SBtab format), describing model and data
  - ``network.jpg`` - thumbnail graphics for BioSimulators website

For examples, see the folders ``examples/example-ecoli-mdf`` and ``examples/example-ecoli-ecm``

## References

1. E. Noor, A. Bar-Even, A. Flamholz, E. Reznik, W. Liebermeister, R. Milo (2014), [Pathway thermodynamics highlights kinetic obstacles in central metabolism](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003483), PLOS Comp. Biol., DOI: 10.1371/journal.pcbi.1003483
2. E. Noor, A. Flamholz, A. Bar-Even, D. Davidi, R. Milo, W. Liebermeister (2016), [*The Protein Cost of Metabolic Fluxes: Prediction from Enzymatic Rate Laws and Cost Minimization*](https://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1005167), PLOS Comp. Biol., DOI: 10.1371/journal.pcbi.1005167
3. T. Lubitz, J. Hahn, F.T. Bergmann, E. Noor, E. Klipp, W. Liebermeister (2016), [SBtab: A flexible table format for data exchange in systems biology](https://academic.oup.com/bioinformatics/article/32/16/2559/1743291?login=false), Bioinformatics 32 (16) 2559–2561
4. F.T. Bergmann, R. Adams, et al. (2014), [COMBINE archive and OMEX format: one file to share all information to reproduce a modeling project](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-014-0369-z). BMC Bioinformatics 15:1. doi:10.1186/s12859-014-0369-z
