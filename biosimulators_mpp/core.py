""" Methods for using COBRApy to execute SED tasks in COMBINE archives and save their outputs

:Author: Elad Noor <elad.noor@weizmann.ac.il>, Wolfram Liebermeister <wolfram.liebermeister@gmail.com>
:Date: March 23, 2022
:Copyright: 2022, Weizmann Institute of Science & INRAE
:License: MIT
"""

from biosimulators_utils.licensing.gurobi import GurobiLicenseManager


GurobiLicenseManager().save_keys_to_license_file()

import copy  # noqa: E402
import os  # noqa: E402

from biosimulators_utils.combine.exec import exec_sedml_docs_in_archive  # noqa: E402
from biosimulators_utils.config import Config, get_config  # noqa: F401, E402
from biosimulators_utils.log.data_model import CombineArchiveLog  # noqa: F401, E402
from biosimulators_utils.log.data_model import StandardOutputErrorCapturerLevel, TaskLog
from biosimulators_utils.model_lang.sbml.utils import (
    get_package_namespace as get_sbml_package_namespace,
)  # noqa: E402
from biosimulators_utils.report.data_model import ReportFormat  # noqa: F401, E402
from biosimulators_utils.report.data_model import SedDocumentResults, VariableResults
from biosimulators_utils.sedml import validation  # noqa: E402
from biosimulators_utils.sedml.data_model import (  # noqa: F401, E402
    ModelAttributeChange,
    ModelLanguage,
    SteadyStateSimulation,
    Task,
    Variable,
)
from biosimulators_utils.sedml.exec import (
    exec_sed_doc as base_exec_sed_doc,
)  # noqa: E402
from biosimulators_utils.simulator.utils import (
    get_algorithm_substitution_policy,
)  # noqa: E402
from biosimulators_utils.utils.core import raise_errors_warnings  # noqa: E402
from biosimulators_utils.viz.data_model import VizFormat  # noqa: F401, E402
from biosimulators_utils.warnings import BioSimulatorsWarning, warn  # noqa: E402
from biosimulators_utils.xml.utils import get_namespaces_for_xml_doc  # noqa: E402
from equilibrator_pathway import (
    EnzymeCostModel,
    Pathway,  # noqa: E402
    ThermodynamicModel,
)
from kisao.data_model import ALGORITHM_SUBSTITUTION_POLICY_LEVELS  # noqa: E402
from kisao.data_model import AlgorithmSubstitutionPolicy
from kisao.utils import get_preferred_substitute_algorithm_by_ids  # noqa: E402
from lxml import etree  # noqa: E402

from .data_model import KISAO_ALGORITHMS_PARAMETERS_MAP  # noqa: E402


__all__ = [
    "exec_sedml_docs_in_combine_archive",
    "exec_sed_doc",
    "exec_sed_task",
]


def exec_sedml_docs_in_combine_archive(archive_filename, out_dir, config=None):
    """Execute the SED tasks defined in a COMBINE/OMEX archive and save the outputs

    Args:
        archive_filename (:obj:`str`): path to COMBINE/OMEX archive
        out_dir (:obj:`str`): path to store the outputs of the archive

            * CSV: directory in which to save outputs to files
              ``{ out_dir }/{ relative-path-to-SED-ML-file-within-archive }/{ report.id }.csv``
            * HDF5: directory in which to save a single HDF5 file (``{ out_dir }/reports.h5``),
              with reports at keys ``{ relative-path-to-SED-ML-file-within-archive }/{ report.id }`` within the HDF5 file

        config (:obj:`Config`, optional): BioSimulators common configuration

    Returns:
        :obj:`tuple`:

            * :obj:`SedDocumentResults`: results
            * :obj:`CombineArchiveLog`: log
    """
    return exec_sedml_docs_in_archive(
        exec_sed_doc,
        archive_filename,
        out_dir,
        apply_xml_model_changes=True,
        config=config,
    )


def exec_sed_doc(
    doc,
    working_dir,
    base_out_path,
    rel_out_path=None,
    apply_xml_model_changes=True,
    log=None,
    indent=0,
    pretty_print_modified_xml_models=False,
    log_level=StandardOutputErrorCapturerLevel.c,
    config=None,
):
    """Execute the tasks specified in a SED document and generate the specified outputs

    Args:
        doc (:obj:`SedDocument` or :obj:`str`): SED document or a path to SED-ML file which defines a SED document
        working_dir (:obj:`str`): working directory of the SED document (path relative to which models are located)

        base_out_path (:obj:`str`): path to store the outputs

            * CSV: directory in which to save outputs to files
              ``{base_out_path}/{rel_out_path}/{report.id}.csv``
            * HDF5: directory in which to save a single HDF5 file (``{base_out_path}/reports.h5``),
              with reports at keys ``{rel_out_path}/{report.id}`` within the HDF5 file

        rel_out_path (:obj:`str`, optional): path relative to :obj:`base_out_path` to store the outputs
        apply_xml_model_changes (:obj:`bool`, optional): if :obj:`True`, apply any model changes specified in the SED-ML file before
            calling :obj:`task_executer`.
        log (:obj:`SedDocumentLog`, optional): log of the document
        indent (:obj:`int`, optional): degree to indent status messages
        pretty_print_modified_xml_models (:obj:`bool`, optional): if :obj:`True`, pretty print modified XML models
        log_level (:obj:`StandardOutputErrorCapturerLevel`, optional): level at which to log output
        config (:obj:`Config`, optional): BioSimulators common configuration
        simulator_config (:obj:`SimulatorConfig`, optional): tellurium configuration

    Returns:
        :obj:`tuple`:

            * :obj:`ReportResults`: results of each report
            * :obj:`SedDocumentLog`: log of the document
    """
    return base_exec_sed_doc(
        exec_sed_task,
        doc,
        working_dir,
        base_out_path,
        rel_out_path=rel_out_path,
        apply_xml_model_changes=apply_xml_model_changes,
        log=log,
        indent=indent,
        pretty_print_modified_xml_models=pretty_print_modified_xml_models,
        log_level=log_level,
        config=config,
    )


def exec_sed_task(task, variables, preprocessed_task=None, log=None, config=None):
    """Execute a task and save its results

    Args:
        task (:obj:`Task`): task
        variables (:obj:`list` of :obj:`Variable`): variables that should be recorded
        preprocessed_task (:obj:`dict`, optional): preprocessed information about the task, including possible
            model changes and variables. This can be used to avoid repeatedly executing the same initialization
            for repeated calls to this method.
        log (:obj:`TaskLog`, optional): log for the task
        config (:obj:`Config`, optional): BioSimulators common configuration

    Returns:
        :obj:`tuple`:

            :obj:`VariableResults`: results of variables
            :obj:`TaskLog`: log

    Raises:
        :obj:`ValueError`: if the task or an aspect of the task is not valid, or the requested output variables
            could not be recorded
        :obj:`NotImplementedError`: if the task is not of a supported type or involves an unsuported feature
    """
    config = config or get_config()

    if config.LOG and not log:
        log = TaskLog()

    if "sbtab_path" not in task["parameters"]:
        raise ValueError(
            "you must specify the SBtab location in `parameters.sbtab_path`"
        )
    sbtab_path = task["parameters"]["sbtab_path"]
    sbtabdoc = SBtabDocument(
        "pathway",
        archive.open(sbtab_path).read().decode(encoding="utf-8"),
        "model.tsv",
    )

    if "algorithm" in task["parameters"]:
        algorithm = task["parameters"]["algorithm"]
    else:  # read which algorithm should be run from the SBtab config table
        _config_sbtab = sbtabdoc.get_sbtab_by_id("Configuration")
        config_df = _config_sbtab.to_data_frame().set_index("Option")
        algorithm = config_df.Value["algorithm"]

    if algorithm == "MDF":
        pathway = ThermodynamicModel.from_sbtab(sbtabdoc)
        if pathway.Nr == 0:
            raise ValueError("Empty pathway")
        logging.info("Starting pathway analysis")
        analysis_results = pathway.mdf_analysis()
    elif algorithm == "ECM":
        ecm_model = EnzymeCostModel.from_sbtab(sbtabdoc)
        pathway = ecm_model._thermo_model
        if pathway.Nr == 0:
            raise ValueError("Empty pathway")
        analysis_results = ecm_model.optimize_ecm()
    else:
        raise ValueError("Input SBtab file does not qualify as MDF nor ECM model")

    # copy results to VariableResults - a dictionary from string to numpy array
    variable_results = VariableResults()

    for _, row in analysis_results.reaction_df.iterrows():
        rid = row["reaction_id"]
        variable_results[f"reacion_{rid}_flux_in_mM_per_s"] = np.array(
            row["flux"].m_as("mM/s")
        )
        for col in [
            "original_standard_dg_prime",
            "standard_dg_prime",
            "physiological_dg_prime",
            "optimized_dg_prime",
        ]:
            variable_results[f"reacion_{rid}_{col}_in_kJ_per_mole"] = np.array(
                row[col].m_as("kJ/mol")
            )

    # format the compound table
    compound_table = analysis_results.compound_df.copy()
    compound_table = compound_table.loc[compound_table.compound_id != "H2O", :]
    for col in ["concentration", "lower_bound", "upper_bound"]:
        cid = row["compound_id"]
        variable_results[f"compound_{cid}_{col}_in_mM"] = np.array(row[col].m_as("mM"))

    # log action
    if config.LOG:
        log.algorithm = algorithm
        log.simulator_details = {
            "algorithm": algorithm,
            "sbtab_path": sbtab_path,
            "sbtab": sbtabdoc.to_str(),
        }

    # Return the results of each variable and log
    return variable_results, log
