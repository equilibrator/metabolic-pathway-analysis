# Combine archive for MDF simulation (E. coli central metablism)

The combine archive (.omex) file is a zip file containing all input and metadata files (see list below). Simulation results (hdf5 file and graphics) can be found in the folder ``results``.

## File provenance

* ``manifest.xml``  - manually written, following an [example](https://biosimulations.org/projects/Escherichia-coli-resource-allocation-Bulovic-Metab-Eng-2019) from biosimulations.org
* ``metadata.rdf``  - manually written, following an [example](https://biosimulations.org/projects/Escherichia-coli-resource-allocation-Bulovic-Metab-Eng-2019) from biosimulations.org
* ``model.tsv``     - copy of the file [ecoli_noor_2016_mdf.tsv](https://gitlab.com/equilibrator/equilibrator-pathway/-/tree/develop/examples/ecoli_noor_2016_mdf.tsv) from the eQuilibrator gitlab repository.
* ``image.jpg``     - network graphics from Noor et al paper

