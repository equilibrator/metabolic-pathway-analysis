docker build -t eladnoor/biosimulators_mpp:0.1 .
docker run -it -v ${PWD}/examples/example-ecoli-ecm:/example eladnoor/biosimulators_mpp:0.1 python -m biosimulators_mpp.cli -i /example/ecoli_noor_2016_ecm.omex -o /example/results/
docker run -it -v ${PWD}/examples/example-ecoli-mdf:/example eladnoor/biosimulators_mpp:0.1 python -m biosimulators_mpp.cli -i /example/ecoli_noor_2016_mdf.omex -o /example/results/

